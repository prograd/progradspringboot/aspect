package com.example.AspectProgram;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class DivisionAspect {

    @Before("execution(* com.example.AspectProgram.Controller.displayResult())")
    public void beforeDivision(){
        System.out.println("Before division");
    }

    @After("execution(* com.example.AspectProgram.Controller.displayResult())")
    public void afterDivision(){
        System.out.println("After division");
    }

}
