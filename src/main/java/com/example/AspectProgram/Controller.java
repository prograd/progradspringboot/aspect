package com.example.AspectProgram;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @Autowired
    DivisionService divisionService;
    @GetMapping("/division")
    public Float displayResult(){
        Division division=new Division(4,2);
        return divisionService.result(division) ;
    }
}
