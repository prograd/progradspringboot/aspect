package com.example.AspectProgram;

import org.springframework.stereotype.Service;

@Service
public class DivisionService {
    public Float result(Division division){
        float res=0;
        try {
              res = (float) division.getFirstNum() / division.getSecondNum();
        }catch(Exception e){
            e.printStackTrace();
        }
        return res;
    }
}
