package com.example.AspectProgram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AspectProgramApplication {

	public static void main(String[] args) {
		SpringApplication.run(AspectProgramApplication.class, args);
	}

}
